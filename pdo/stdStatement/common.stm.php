<?php namespace pdo\stdStatement;

use pdo\Connection\MysqlConnect;

class Common {
    function __construct()
    {
        $this->conn = MysqlConnect::connect();
    }
}