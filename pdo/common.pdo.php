<?php namespace pdo;

use pdo\Connection\MysqlConnect;

class Common
{
 public function __construct()
 {
  $this->conn = MysqlConnect::connect();
 }
}
