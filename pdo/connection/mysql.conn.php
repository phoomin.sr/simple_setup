<?php namespace pdo\Connection;
require_once dirname(__FILE__) . "/config.php";

use PDO;

class MysqlConnect {
    public static function connect()
    {
        $conn = new PDO("mysql:host=".host.";dbname=".dbname, username, password);
        return $conn;
    }
}