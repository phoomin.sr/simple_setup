<?php

define('ROOTS', 'http://localhost/simple_setup/');
define("RDIR", dirname(__FILE__));
define("TZ", "Asia/Bangkok");

spl_autoload_register(function ($class) {
    $lowerClass = '/' . strtolower(str_replace("\\", "/", $class));
    /**
     * load library
     */
    if (is_file(RDIR . $lowerClass . ".component.php")) {
        require_once RDIR . $lowerClass . '.component.php';
    }
    /**
     * load connection
     */
    if (is_file(RDIR . $lowerClass . ".conn.php")) {
        require_once RDIR . $lowerClass . '.conn.php';
    }
    /**
     * load stdStatement
     */
    if (is_file(RDIR . $lowerClass . ".stm.php")) {
        require_once RDIR . $lowerClass . '.stm.php';
    }
    /**
     * load pdo
     */
    if (is_file(RDIR . $lowerClass . ".pdo.php")) {
        require_once RDIR . $lowerClass . '.pdo.php';
    }
    /**
     * load request
     */
    if (is_file(RDIR . $lowerClass . ".request.php")) {
        require_once RDIR . $lowerClass . '.request.php';
    }
    /**
     * load views
     */
    if (is_file(RDIR . $lowerClass . ".views.php")) {
        require_once RDIR . $lowerClass . '.views.php';
    }
});
