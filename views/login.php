<?php
require_once dirname(__DIR__) . "/config.php";

use Views\CommonViews;

$login = new CommonViews('login');
$login->setScript();
$login->setStyle();

echo $login->head();