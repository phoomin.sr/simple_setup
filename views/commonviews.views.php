<?php namespace Views;

class CommonViews {
    public $head = "";
    public $title = "";
    
    function __construct($title)
    {
        $this->title = $title;
        return $this;
    }
    function setScript($script)
    {
        $this->script[] = $script;
        return $this;
    }
    function setStyle($style)
    {
        $this->style[] = $style;
        return $this;
    }
    function getScript () 
    {
        $this->rawscript = "";
        for ($i = 0;$i < count($this->script);$i++) {
           $this->rawScript .= "<script src='".ROOTS . $this->script."'></script>"; 
        }
        return $this;
    }
    function getStyle () 
    {
        $this->rawStyle = "";
        for ($i = 0;$i < count($this->style);$i++) {
           $this->rawStyle .= "<link rel='stylesheet' href='".ROOTS . $this->style."'></link>"; 
        }
        return $this;
    }
    function head()
    {
        $this->head .= "<head>"
            . "<title>" . $this->title . "</title>"
            . $this->getScript()
            . $this->getStyle()
            . "</head>";

        return $this->head();
    }
    function body($callback)
    {
        $this->body = "<body>"
        . $callback()
        . "</body>";

        return $this->body;
    }
}